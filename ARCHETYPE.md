##Project goal

Archetype provides the template for base spring boot project.

##How to build

In order to compile this sources you need to install:

1. Maven 3 >
2. Java 1.8.100 >

```xml
<servers>
    <server>
        <id>magora-repository</id>
        <username>deployment</username>
        <password>password</password>
    </server>
</servers>
```

##Create project from archetype

```
    mvn archetype:generate -Dversion=1.0.0 -DgroupId=com.mgr.template -DartifactId=template  -DarchetypeVersion=2.0.0 -DarchetypeGroupId=com.magora-systems.archetype -DarchetypeArtifactId=standard-spring-boot-project  -DinteractiveMode=false
```

##Create a new version of archetype

In order to create a new version archetype the following actions should be done

1. Update version in archetype.properties -> archetype.version
2. Create archetype from code
```
    mvn archetype:create-from-project -Darchetype.properties=./archetype.properties
```
3. Deploy the archetype to nexus
```
    cd target/generated-sources/archetype
    mvn clean deploy -DaltDeploymentRepository=magora-repository::default::http://nexus.java.magora.team/content/repositories/releases/
```
