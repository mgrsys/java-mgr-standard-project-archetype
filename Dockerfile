FROM java:8-jre
MAINTAINER Fyodor Kemenov <kemenov@magora.systems>

ENV SPRING_CONFIG_ADDITIONAL_LOCATION app/.config/
ENV JAVA_ARG -Xmx400m -XX:+ExitOnOutOfMemoryError -Djava.security.egd=file:/dev/./urandom

ADD ./app/target/${parentArtifactId}-app.jar /app/
CMD java ${JAVA_ARG} -jar  ./app/${parentArtifactId}-app.jar
