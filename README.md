##Project goal

The standard project. Features:
* Registration;
* Authentication by email/psw;
* Authorization by token;
* Refresh token;


##How to build

In order to compile this sources you need to install:

1. Maven 3 >
2. Java 1.8.100 >
3. Docker 1.11.1 >

```
cd PROJECT_DIR
```

Build source code

```
mvn clean package
```

Test 

```
mvn clean test -PwithIntegrationTests, -Dspring.profiles.active=testcontainers
```

Build and make docker images

```
mvn clean package -Pdocker
```

Build docker and push to registry

```
mvn clean install -Pdocker
```

Build docker images with custom image

```
mvn clean package -Pdocker -Ddocker.image-tag=1.0.0 -Pdocker
```

Run docker images

```
docker run -p 8080:8080 --name ${artifactId} ${groupId}/${artifactId}-app
```
