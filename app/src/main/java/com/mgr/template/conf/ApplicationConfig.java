package com.mgr.template.conf;

import com.magorasystems.protocolapi.spring.GlobalExceptionHandlerController;
import com.magorasystems.protocolapi.spring.SpringMVCProtocolErrorResolver;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.web.method.annotation.AuthenticationPrincipalArgumentResolver;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */

@Configuration
@ComponentScan(basePackages = {"com.mgr.template.facade.controller", "com.mgr.template.service", "com.magorasystems.protocolapi.spring"})
@EnableJpaRepositories(basePackages = {"com.mgr.template.service.repository"})
@EntityScan(basePackages = {"com.magorasystems.hibernate.pg.entity", "com.mgr.template.service.entity", "org.springframework.data.jpa.convert.threeten"})
@EnableConfigurationProperties
public class ApplicationConfig {

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }


    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public CommonsRequestLoggingFilter requestLoggingFilter() {
        CommonsRequestLoggingFilter crlf = new CommonsRequestLoggingFilter();
        crlf.setIncludeClientInfo(true);
        crlf.setIncludeQueryString(true);
        crlf.setIncludePayload(true);
        crlf.setIncludeHeaders(true);
        return crlf;
    }


    @Bean
    public SpringMVCProtocolErrorResolver protocolErrorResolver() {
        SpringMVCProtocolErrorResolver protocolErrorResolver = new SpringMVCProtocolErrorResolver();
        protocolErrorResolver.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return protocolErrorResolver;
    }


    @Bean
    public ErrorController errorController() {
        return new GlobalExceptionHandlerController();
    }


    @Configuration
    public class MvcConfig implements WebMvcConfigurer {

        @Bean
        public AuthenticationPrincipalArgumentResolver authenticationPrincipalArgumentResolver() {
            return new AuthenticationPrincipalArgumentResolver();
        }


        @Override
        public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
            argumentResolvers.add(authenticationPrincipalArgumentResolver());
        }

        @Override
        public void addInterceptors(InterceptorRegistry registry) {
//            registry.addInterceptor(new VersionSpecificInterceptor());
        }

    }

}
