package com.mgr.template.conf;

import com.magorasystems.authapi.spring.security.ProtocolAccessDeniedHandler;
import com.magorasystems.authapi.spring.security.ProtocolAuthenticationEntryPoint;
import com.magorasystems.authapi.spring.security.ProtocolSecurityExceptionRenderer;
import com.magorasystems.authapi.spring.security.ProtocolSecurityExceptionRendererImpl;
import com.magorasystems.authapi.spring.security.jwt.DefaultJwtAuthenticationFilter;
import com.mgr.template.security.AuthenticationProvider;
import com.mgr.template.service.AuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.filter.CorsFilter;


@Configuration
@Order(SecurityProperties.BASIC_AUTH_ORDER)
public class SpringSecurityConfigurer extends WebSecurityConfigurerAdapter {

    private final static String AUTH_PREFIX = "/api/v*";

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private CommonsRequestLoggingFilter commonsRequestLoggingFilter = new CommonsRequestLoggingFilter();


    @Autowired
    private CorsFilter corsFilter;

    @Autowired
    private AuthorizationService authorizationService;

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers(AUTH_PREFIX + "/registration/**")
                .antMatchers("/v2/api-docs")
                .antMatchers(HttpMethod.POST, AUTH_PREFIX + "/auth/token")
                .antMatchers(HttpMethod.PUT, AUTH_PREFIX + "/auth/token");
    }


    @Bean
    public AuthenticationProvider authProvider() {
        return new AuthenticationProvider(authorizationService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        DefaultJwtAuthenticationFilter customJwtAuthenticationFilter = new DefaultJwtAuthenticationFilter();
        customJwtAuthenticationFilter.setAuthenticationManager(authenticationManager());

        ProtocolSecurityExceptionRenderer securityErrorRenderer = new ProtocolSecurityExceptionRendererImpl();
        ProtocolAuthenticationEntryPoint jwtAuthenticationEntryPoint = new ProtocolAuthenticationEntryPoint(securityErrorRenderer);
        customJwtAuthenticationFilter.setAuthenticationEntryPoint(jwtAuthenticationEntryPoint);

        http
                .csrf().disable()
                .anonymous()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(jwtAuthenticationEntryPoint)
                .accessDeniedHandler(new ProtocolAccessDeniedHandler(securityErrorRenderer))
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/api/v*/**").permitAll()
                .antMatchers("/api/v*/**").authenticated();

        http
                .addFilterBefore(customJwtAuthenticationFilter, BasicAuthenticationFilter.class)
                .addFilterBefore(corsFilter, DefaultJwtAuthenticationFilter.class)
                .addFilterBefore(commonsRequestLoggingFilter, BasicAuthenticationFilter.class);


    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider());
    }


}