package com.mgr.template.conf;


import com.fasterxml.classmate.TypeResolver;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.google.common.base.Optional;
import com.mgr.template.domain.SystemUser;
import com.mgr.template.domain.jackson.EncodedId;
import com.mgr.template.security.AuthorizedUser;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.info.ProjectInfoProperties;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.schema.ModelPropertyBuilderPlugin;
import springfox.documentation.spi.schema.contexts.ModelPropertyContext;
import springfox.documentation.spi.service.ExpandedParameterBuilderPlugin;
import springfox.documentation.spi.service.ParameterBuilderPlugin;
import springfox.documentation.spi.service.contexts.ParameterContext;
import springfox.documentation.spi.service.contexts.ParameterExpansionContext;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import static com.google.common.collect.Lists.newArrayList;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@Configuration
@EnableSwagger2
@Slf4j
public class SwaggerConfig {


    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String API_KEY_NAME = "apiKey";

    @Autowired(required = false)
    private BuildProperties buildProperties;
    @Autowired(required = false)
    private ProjectInfoProperties projectInfoProperties;

    @Value("${spring.application.name}")
    private String appName;

    @Bean
    public Docket publicApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("public-api")
                .select()
                .paths(PathSelectors.ant("/api/**"))
                .paths(PathSelectors.any())
                .build()
                // unfortunately swagger doesn't convert these types correctly
                .directModelSubstitute(LocalDate.class, String.class)
                .directModelSubstitute(LocalTime.class, String.class)
                .directModelSubstitute(ZonedDateTime.class, String.class)
                .useDefaultResponseMessages(false)
                .securityContexts(newArrayList(securityContext()))
                .securitySchemes(newArrayList(apiKey()))
                .ignoredParameterTypes(SystemUser.class, AuthorizedUser.class)
                .apiInfo(apiInfo());
    }


    private ApiInfo apiInfo() {
        return new ApiInfo(
                appName,
                composeDescription(),
                buildProperties == null ? null : buildProperties.getVersion(),
                null,
                null,
                null,
                null,
                Collections.emptyList());
    }

    private ApiKey apiKey() {
        return new ApiKey(API_KEY_NAME, AUTHORIZATION_HEADER, "header");
    }

    @SneakyThrows
    private String composeDescription() {


        StringBuilder sb = new StringBuilder();
        sb.append("Interactive API reference for ").append(appName).append(" system").append("\n\n");

        Resource buildRes = projectInfoProperties.getBuild().getLocation();
        if (buildRes.exists()) {
            Properties build = loadFrom(buildRes, "build");
            sb.append("Build version: ").append(buildProperties.getVersion()).append("\n");
            sb.append("Build time: ").append(build.getProperty("time")).append("\n\n\n");
        }

        Resource gitRes = projectInfoProperties.getGit().getLocation();
        if (gitRes.exists()) {
            Properties git = loadFrom(gitRes, "git");
            sb.append("Git time: ").append(git.getProperty("commit.time")).append("\n");
            sb.append("Git author: ").append(git.getProperty("commit.user.name")).append("\n");
            sb.append("Git comment: ").append(git.getProperty("commit.message.full")).append("\n");
            sb.append("Git commit: ").append(git.getProperty("commit.id.abbrev"));
        }


        return sb.toString();
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.any())
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[]{
                new AuthorizationScope("global", "accessEverything")
        };
        return Collections.singletonList(new SecurityReference(API_KEY_NAME, authorizationScopes));
    }


    private Properties loadFrom(org.springframework.core.io.Resource location, String prefix) throws IOException {
        String p = prefix.endsWith(".") ? prefix : prefix + ".";
        Properties source = PropertiesLoaderUtils.loadProperties(location);
        Properties target = new Properties();
        for (String key : source.stringPropertyNames()) {
            if (key.startsWith(p)) {
                target.put(key.substring(p.length()), source.get(key));
            }
        }
        return target;
    }

    @Bean
    SecurityConfiguration security() {
        return SecurityConfigurationBuilder.builder()
                .scopeSeparator(",")
                .useBasicAuthenticationWithAccessCodeGrant(false)
                .build();
    }

    @Bean
    ExpandedParameterBuilderPlugin modelBuilderPlugin(
            TypeResolver resolver
    ) {
        return new ExpandedParameterBuilderPlugin() {

            @Override
            public boolean supports(DocumentationType documentationType) {
                return true;
            }

            @Override
            public void apply(ParameterExpansionContext context) {
                EncodedId annotation = context.getField().getRawMember().getAnnotation(EncodedId.class);
                if (annotation != null) {
                    context.getParameterBuilder()
                            .type(resolver.resolve(String.class))
                            .modelRef(new ModelRef("string"))
                    ;
                }
            }
        };
    }

    @Bean
    ParameterBuilderPlugin parameterBuilderPlugin(
            TypeResolver resolver
    ) {
        return new ParameterBuilderPlugin() {
            @Override
            public boolean supports(DocumentationType documentationType) {
                return true;
            }

            @Override
            public void apply(ParameterContext parameterContext) {
                ResolvedMethodParameter resolvedMethodParameter = parameterContext.resolvedMethodParameter();
                if (resolvedMethodParameter.hasParameterAnnotation(EncodedId.class)){
                    parameterContext.parameterBuilder()
                            .type(resolver.resolve(String.class))
                            .modelRef(new ModelRef("string"))
                    ;
                }
            }
        };
    }

    @Bean
    ModelPropertyBuilderPlugin modelPropertyBuilderPlugin(
            TypeResolver resolver
    ) {
        return new ModelPropertyBuilderPlugin() {
            @Override
            public boolean supports(DocumentationType documentationType) {
                return true;
            }

            @Override
            public void apply(ModelPropertyContext context) {

                if (context.getBeanPropertyDefinition().isPresent()) {
                    BeanPropertyDefinition bpd = context.getBeanPropertyDefinition().get();
                    EncodedId annotation = bpd.getField().getAnnotation(EncodedId.class);
                    if (annotation != null) {
                        context.getBuilder()
                                .type(resolver.resolve(String.class))
                                .example(annotation.value().getWithPrefix("xyz"));
                    }
                }
            }
        };
    }

}