CREATE TABLE sys_users (
  id         BIGSERIAL PRIMARY KEY    NOT NULL,
  email      TEXT                     NOT NULL CHECK (char_length(email) <= 255),
  created_at TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE principal_secret (
  user_id      BIGINT PRIMARY KEY       NOT NULL REFERENCES sys_users,
  refresh_time TIMESTAMP WITH TIME ZONE NOT NULL,
  secret_code  TEXT                     NOT NULL CHECK (char_length(secret_code) <= 255)
);

CREATE TABLE login_password_principal (
  user_id    BIGINT PRIMARY KEY       NOT NULL REFERENCES sys_users,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL,
  login      TEXT                     NOT NULL CHECK (char_length(login) <= 255),
  password   TEXT                     NOT NULL CHECK (char_length(password) <= 255)
);
CREATE UNIQUE INDEX login_password_principal_login_unq
  ON login_password_principal USING BTREE (login);

