package com.mgr.template.domain;

import com.mgr.template.domain.jackson.EncodedId;
import com.mgr.template.domain.jackson.EncodedIdType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Hello {

    @EncodedId(EncodedIdType.USER)
    private Long userId;

    private String helloMessage;

}
