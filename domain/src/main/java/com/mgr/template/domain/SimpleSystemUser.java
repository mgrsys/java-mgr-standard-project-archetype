package com.mgr.template.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;


/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class SimpleSystemUser implements SystemUser, Serializable {

    private final Long userId;

    @JsonCreator
    public SimpleSystemUser(
            @JsonProperty("userId") Long userId) {
        this.userId = userId;
    }

}
