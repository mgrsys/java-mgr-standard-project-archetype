package com.mgr.template.domain;

import java.util.Date;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
public interface SystemUser {

    Long getUserId();

}
