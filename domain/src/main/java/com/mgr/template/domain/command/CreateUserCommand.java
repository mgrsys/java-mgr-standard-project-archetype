package com.mgr.template.domain.command;

import com.magorasystems.protocolapi.request.auth.ClientAuthMeta;
import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@Data
public class CreateUserCommand {

    @NotBlank
    @Length(min = FieldPatternConstraint.USER_FULL_NAME_MIN_LENGTH, max = FieldPatternConstraint.USER_FULL_NAME_MAX_LENGTH)
    private String name;

    @NotBlank
    @Length(min = FieldPatternConstraint.USER_PASSWORD_MIN_LENGTH, max = FieldPatternConstraint.USER_PASSWORD_MAX_LENGTH)
    private String password;

    @Email
    @NotBlank
    private String email;

    private ClientAuthMeta meta;


}
