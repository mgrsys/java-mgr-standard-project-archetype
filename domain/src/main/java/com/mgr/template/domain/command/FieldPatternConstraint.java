package com.mgr.template.domain.command;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
public final class FieldPatternConstraint {


    public static final int USER_FULL_NAME_MIN_LENGTH = 1;
    public static final int USER_FULL_NAME_MAX_LENGTH = 30;
    public static final int USER_PASSWORD_MIN_LENGTH = 6;
    public static final int USER_PASSWORD_MAX_LENGTH = 32;

}

