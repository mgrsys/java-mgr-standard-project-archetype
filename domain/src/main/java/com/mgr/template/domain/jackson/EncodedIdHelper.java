package com.mgr.template.domain.jackson;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
public interface EncodedIdHelper {
    String encode(Long value, EncodedIdType idType);

    Long decode(String value, EncodedIdType idType);
}
