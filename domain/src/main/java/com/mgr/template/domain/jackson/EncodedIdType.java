package com.mgr.template.domain.jackson;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
public enum EncodedIdType {
    USER("us");

    private static final char DELIMITER = '_';

    private final String prefix;
    private final String prefixWithDelimiter;

    EncodedIdType(String prefix) {
        this.prefix = prefix;
        this.prefixWithDelimiter = prefix + DELIMITER;
    }

    public String getWithPrefix(String rawId) {
        return prefixWithDelimiter + rawId;
    }

    public String getWithoutPrefix(String idWithPrefix) {
        return idWithPrefix.replaceFirst(this.prefixWithDelimiter, "");
    }

    public static String parseByType(String text, EncodedIdType idType) throws WrongIdRequestedException, WrongIdTypeRequestedException {
        String currentPrefix = text.substring(0, text.indexOf(DELIMITER));

        EncodedIdType id = Stream.of(EncodedIdType.values())
                .filter(str -> str.prefix.equalsIgnoreCase(currentPrefix))
                .findFirst()
                .orElseThrow(() -> new WrongIdRequestedException(idType));

        return Optional.of(id)
                .filter(idType::equals)
                .map(actual -> actual.getWithoutPrefix(text))
                .orElseThrow(() -> new WrongIdTypeRequestedException(id, idType));
    }

}
