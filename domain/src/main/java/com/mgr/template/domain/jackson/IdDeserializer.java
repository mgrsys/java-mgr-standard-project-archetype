package com.mgr.template.domain.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import lombok.AccessLevel;
import lombok.Setter;

import java.io.IOException;
import java.util.Optional;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
class IdDeserializer extends StdDeserializer<Long> implements ContextualDeserializer {

    private final EncodedIdHelper idHelper;

    @Setter(AccessLevel.PRIVATE)
    private EncodedIdType id;

    private IdDeserializer(EncodedIdHelper idHelper) {
        super(Long.class);
        this.idHelper = idHelper;
        this.id = null;
    }

    @Override
    public Long deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        if (p.hasToken(JsonToken.VALUE_STRING)) {
            return idHelper.decode(p.getText(), id);
        }
        return (Long) ctxt.handleUnexpectedToken(Long.class, p);
    }

    @Override
    public JsonDeserializer<?> createContextual(DeserializationContext ctxt, BeanProperty property) throws JsonMappingException {
        return Optional
                .ofNullable(property)
                .filter(this::isLongField)
                .map(p -> p.getAnnotation(EncodedId.class))
                .map(EncodedId::value)
                .map(this::buildByType)
                .orElseThrow(() -> JsonMappingException.from(ctxt, "EncodedIdType was not provided"));
    }

    private boolean isLongField(BeanProperty property) {
        return Optional
                .ofNullable(property)
                .map(BeanProperty::getType)
                .map(JavaType::getRawClass)
                .filter(type -> type.equals(Long.class))
                .isPresent();
    }

    private IdDeserializer buildByType(EncodedIdType type) {
        IdDeserializer deserializer = this;
        if (type != this.id) {
            deserializer = new IdDeserializer(idHelper);
            deserializer.setId(type);
        }
        return deserializer;
    }

}
