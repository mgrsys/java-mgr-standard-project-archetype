package com.mgr.template.domain.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.AccessLevel;
import lombok.Setter;

import java.io.IOException;
import java.util.Optional;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
class IdSerializer extends StdSerializer<Long> implements ContextualSerializer {

    private final EncodedIdHelper idHelper;

    @Setter(AccessLevel.PRIVATE)
    private EncodedIdType id;

    private IdSerializer(EncodedIdHelper idHelper) {
        super(Long.class);
        this.idHelper = idHelper;
        this.id = null;
    }

    @Override
    public void serialize(Long value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        if (id != null) {
            String encoded = idHelper.encode(value, id);
            gen.writeString(encoded);
        } else {
            throw JsonMappingException.from(gen, "idType should be provided");
        }
    }

    @Override
    public JsonSerializer<Long> createContextual(SerializerProvider prov, BeanProperty property) throws JsonMappingException {
        return Optional
                .ofNullable(property)
                .filter(this::isLongField)
                .map(p -> p.getAnnotation(EncodedId.class))
                .map(EncodedId::value)
                .map(this::buildByType)
                .orElseThrow(() -> JsonMappingException.from(prov, "EncodedIdType was not provided"));
    }

    private boolean isLongField(BeanProperty property) {
        return Optional
                .ofNullable(property)
                .map(BeanProperty::getType)
                .map(JavaType::getRawClass)
                .filter(type -> type.equals(Long.class))
                .isPresent();
    }

    private IdSerializer buildByType(EncodedIdType type) {
        IdSerializer serializer = this;
        if (type != this.id) {
            serializer = new IdSerializer(idHelper);
            serializer.setId(type);
        }
        return serializer;
    }

}
