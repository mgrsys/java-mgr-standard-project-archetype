package com.mgr.template.domain.jackson;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@Getter @RequiredArgsConstructor
public class WrongIdTypeRequestedException extends Exception {

    private final EncodedIdType actualIdType;

    private final EncodedIdType requestedIdType;

}
