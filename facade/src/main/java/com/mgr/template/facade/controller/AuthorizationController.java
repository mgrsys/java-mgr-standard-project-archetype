package com.mgr.template.facade.controller;


import com.magorasystems.protocolapi.request.auth.ClientAuthRequest;
import com.magorasystems.protocolapi.request.auth.RefreshTokenRequest;
import com.magorasystems.protocolapi.response.ResponseCodes;
import com.magorasystems.protocolapi.response.SuccessEmptyResponse;
import com.magorasystems.protocolapi.response.auth.AuthResponseData;
import com.magorasystems.protocolapi.response.auth.AuthSuccessResponse;
import com.magorasystems.protocolapi.response.auth.StringAuthInfo;
import com.mgr.template.domain.SystemUser;
import com.mgr.template.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@RestController
@RequestMapping(value = "/api/${app.api.version}/auth")
class AuthorizationController {

    private final AuthenticationService authenticationService;

    @Autowired
    public AuthorizationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @RequestMapping(value = "/token", method = RequestMethod.POST)
    public AuthSuccessResponse<AuthResponseData<StringAuthInfo>> createToken(@RequestBody ClientAuthRequest simpleAuthRequest) throws Exception {

        AuthResponseData<StringAuthInfo> authorize = authenticationService.authenticateUser(
                simpleAuthRequest
        );

        return new AuthSuccessResponse<>(ResponseCodes.SUCCESS_CODE, authorize);
    }

    @RequestMapping(value = "/token", method = RequestMethod.PUT)
    public AuthSuccessResponse<AuthResponseData<StringAuthInfo>> refreshToken(@RequestBody RefreshTokenRequest refreshTokenRequest) throws Exception {
        AuthResponseData<StringAuthInfo> token = authenticationService.refreshToken(null, refreshTokenRequest.getRefreshToken());
        return new AuthSuccessResponse<>(ResponseCodes.SUCCESS_CODE, token);
    }

    @RequestMapping(value = "/token", method = RequestMethod.DELETE)
    public SuccessEmptyResponse signOutFromCurrentToken(@AuthenticationPrincipal SystemUser userKey) {
        return SuccessEmptyResponse.getInstance();
    }

    @RequestMapping(value = "/tokens", method = RequestMethod.DELETE)
    public SuccessEmptyResponse signOutFromAllTokens(@AuthenticationPrincipal SystemUser userKey) {
        authenticationService.signOutAllDevices(userKey);
        return SuccessEmptyResponse.getInstance();
    }


}
