package com.mgr.template.facade.controller;

import com.magorasystems.protocolapi.response.SuccessResponse;
import com.mgr.template.domain.Hello;
import com.mgr.template.domain.SystemUser;
import com.mgr.template.domain.jackson.EncodedId;
import com.mgr.template.domain.jackson.EncodedIdType;
import com.mgr.template.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */

@RestController
@RequestMapping(value = "/api/${app.api.version}/hello")
public class HelloController {

    private final HelloService helloService;

    @Autowired
    public HelloController(HelloService helloService) {
        this.helloService = helloService;
    }

    @GetMapping
    public SuccessResponse<Hello> sayHello(
            @AuthenticationPrincipal SystemUser user
    ) {
        return new SuccessResponse<>(helloService.sayHello(user.getUserId()));
    }


    @GetMapping("/users")
    public SuccessResponse<Hello> sayHelloUserParam(Hello data) {
        return new SuccessResponse<>(helloService.sayHello(data.getUserId()));
    }

    @GetMapping("/users/{userId}")
    public SuccessResponse<Hello> sayHelloUserPath(
            @PathVariable @EncodedId(EncodedIdType.USER) Long userId
    ) {
        return new SuccessResponse<>(helloService.sayHello(userId));
    }

}
