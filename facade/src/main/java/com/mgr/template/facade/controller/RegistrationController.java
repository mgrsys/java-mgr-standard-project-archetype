package com.mgr.template.facade.controller;

import com.magorasystems.protocolapi.response.ResponseCodes;
import com.magorasystems.protocolapi.response.auth.AuthResponseData;
import com.magorasystems.protocolapi.response.auth.AuthSuccessResponse;
import com.magorasystems.protocolapi.response.auth.StringAuthInfo;
import com.mgr.template.domain.command.CreateUserCommand;
import com.mgr.template.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@RestController
@RequestMapping("/api/${app.api.version}/registration")
public class RegistrationController {

    private final RegistrationService registrationService;

    @Autowired
    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public AuthSuccessResponse<AuthResponseData<StringAuthInfo>> createNewUser(
            @RequestBody CreateUserCommand createUserCommand) {

        return new AuthSuccessResponse<>(ResponseCodes.SUCCESS_CODE, registrationService.createUser(createUserCommand));
    }
}
