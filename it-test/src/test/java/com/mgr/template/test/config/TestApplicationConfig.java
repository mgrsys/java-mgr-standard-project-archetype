package com.mgr.template.test.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@Configuration
@SpringBootApplication
@ComponentScan(basePackages = {"com.mgr.template.service", "com.mgr.template.test.helper"})
@EnableJpaRepositories(basePackages = {"com.mgr.template.service.repository"})
@EntityScan(basePackages = {"com.magorasystems.hibernate.pg.entity", "com.mgr.template.service.entity", "org.springframework.data.jpa.convert.threeten"})
@EnableConfigurationProperties
@Slf4j
public class TestApplicationConfig {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public MethodValidationPostProcessor validationPostProcessor() {
        return new MethodValidationPostProcessor();
    }



}
