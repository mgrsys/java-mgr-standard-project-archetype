package com.mgr.template.test.helper;

import com.magorasystems.protocolapi.response.auth.AuthResponseData;
import com.magorasystems.protocolapi.response.auth.StringAuthInfo;
import com.mgr.template.domain.command.CreateUserCommand;
import com.mgr.template.domain.jackson.EncodedIdHelper;
import com.mgr.template.domain.jackson.EncodedIdType;
import com.mgr.template.service.RegistrationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@Slf4j
@Component
public class UserTestHelper {

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private EncodedIdHelper encodedIdHelper;

    @Transactional
    public Long createUser(String name, String email, String psw) {
        CreateUserCommand uc = new CreateUserCommand();
        uc.setName(name);
        uc.setEmail(email);
        uc.setPassword(psw);
        AuthResponseData<StringAuthInfo> user = registrationService.createUser(uc);
        return encodedIdHelper.decode(user.getAuthInfo().getUserId(), EncodedIdType.USER);
    }

}