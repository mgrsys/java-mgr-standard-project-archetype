package com.mgr.template.test.service;

import com.mgr.template.test.config.TestApplicationConfig;
import com.mgr.template.test.config.TestDBConfig;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TestApplicationConfig.class, TestDBConfig.class})
abstract class AbstractBaseDockerTest {
}