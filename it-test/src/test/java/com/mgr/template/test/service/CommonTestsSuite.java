package com.mgr.template.test.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        RegistrationUserTest.class
})
public class CommonTestsSuite {


}