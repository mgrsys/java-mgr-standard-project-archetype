package com.mgr.template.test.service;

import com.magorasystems.authapi.exception.WrongCredentialsDetailsException;
import com.magorasystems.protocolapi.exception.impl.ResourceAlreadyExistException;
import com.magorasystems.protocolapi.request.auth.ClientAuthRequest;
import com.mgr.template.service.AuthenticationService;
import com.mgr.template.service.repository.UserRepository;
import com.mgr.template.test.helper.UserTestHelper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = {"/sql/ddl-after-test.sql"})
})
@Slf4j
public class RegistrationUserTest extends AbstractBaseDockerTest {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UserTestHelper userTestHelper;

    @Autowired
    private UserRepository userRepository;

    private static final String NAME = "TestUser";
    private static final String EMAIl = "test@email.com";
    private static final String PSW = "password";

    @Test
    public void test__create_user() {
        Assert.assertNotNull(userTestHelper.createUser(NAME, EMAIl, PSW));
    }

    @Test
    public void test__auth_user() throws WrongCredentialsDetailsException {
        Assert.assertNotNull(userTestHelper.createUser(NAME, EMAIl, PSW));
        Assert.assertNotNull(
                authenticationService.authenticateUser(
                        new ClientAuthRequest(EMAIl, PSW, null)
                )
        );
    }

    @Test(expected = WrongCredentialsDetailsException.class)
    public void test__auth_user_fail_psw() throws WrongCredentialsDetailsException {
        Assert.assertNotNull(userTestHelper.createUser(NAME, EMAIl, PSW));
        Assert.assertNotNull(
                authenticationService.authenticateUser(
                        new ClientAuthRequest(EMAIl, "failpsw", null)
                )
        );
    }

    @Test
    public void test__auth_user_with_role() throws WrongCredentialsDetailsException {
        Assert.assertNotNull(userTestHelper.createUser(NAME, EMAIl, PSW));
        Assert.assertNotNull(
                authenticationService.authenticateUser(
                        new ClientAuthRequest(EMAIl, PSW, null)
                )
        );
        Assert.assertNotNull(
                authenticationService.authenticateUser(
                        new ClientAuthRequest(EMAIl, PSW, null)
                )
        );
    }

    @Test
    public void test__create_several_user() {
        Assert.assertNotNull(userTestHelper.createUser(NAME, EMAIl, PSW));
        Assert.assertNotNull(userTestHelper.createUser(NAME, "test2@email.com", PSW));
        Assert.assertEquals(2, userRepository.count());
    }

    @Test(expected = ResourceAlreadyExistException.class)
    public void test__create_duplicated_user() {
        Assert.assertNotNull(userTestHelper.createUser(NAME, EMAIl, PSW));
        userTestHelper.createUser(NAME, EMAIl, PSW);
    }


}