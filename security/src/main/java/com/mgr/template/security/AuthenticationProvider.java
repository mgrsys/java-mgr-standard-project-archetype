package com.mgr.template.security;

import com.mgr.template.domain.SystemUser;
import com.mgr.template.service.AuthorizationService;

import com.magorasystems.authapi.exception.InvalidTokenException;
import com.magorasystems.authapi.spring.security.*;
import com.magorasystems.protocolapi.response.auth.AuthResponseCodes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;


public class AuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    private final AuthorizationService authorizationService;

    @Autowired
    public AuthenticationProvider(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }


    @Override
    public boolean supports(Class<?> authentication) {
        return (TokenAuthenticationExtractorImpl.AuthenticationTokenHolder.class.isAssignableFrom(authentication));
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

        TokenAuthenticationExtractorImpl.AuthenticationTokenHolder jwtAuthenticationToken = (TokenAuthenticationExtractorImpl.AuthenticationTokenHolder) authentication;

        try {
            SystemUser authorize = authorizationService.authorizeUser(jwtAuthenticationToken.getToken());
            return new AuthorizedUser(authorize.getUserId());
        } catch (InvalidTokenException e) {
            throw new SpringAuthenticationException(e);
        } catch (Exception e) {
            throw new SpringAuthenticationException(
                    InvalidTokenException.TOKEN_FIELD,
                    AuthResponseCodes.ACCESS_TOKEN_INVALID_ERROR,
                    e.getMessage()
            );
        }

    }

}