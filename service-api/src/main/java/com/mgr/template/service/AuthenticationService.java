package com.mgr.template.service;


import com.magorasystems.authapi.exception.UpdateAccessTokenException;
import com.magorasystems.authapi.exception.WrongCredentialsDetailsException;
import com.magorasystems.protocolapi.request.auth.ClientAuthRequest;
import com.magorasystems.protocolapi.response.auth.AuthResponseData;
import com.magorasystems.protocolapi.response.auth.StringAuthInfo;
import com.mgr.template.domain.SystemUser;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
public interface AuthenticationService {

    AuthResponseData<StringAuthInfo> authenticateUser(@Valid ClientAuthRequest simpleAuthRequest) throws WrongCredentialsDetailsException;

    AuthResponseData<StringAuthInfo> refreshToken(String accessToken, @NotNull String refreshToken) throws UpdateAccessTokenException;

    void signOutAllDevices(SystemUser key);

}
