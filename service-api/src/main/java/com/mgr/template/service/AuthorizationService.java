package com.mgr.template.service;


import com.mgr.template.domain.SystemUser;
import com.magorasystems.authapi.exception.InvalidTokenException;


/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
public interface AuthorizationService {

    SystemUser authorizeUser(String token) throws InvalidTokenException;

}
