package com.mgr.template.service;

import com.mgr.template.domain.Hello;

/**
* @author Developed by Magora Team (magora-systems.com). 2018.
 */

public interface HelloService {

    Hello sayHello(Long userId);

}
