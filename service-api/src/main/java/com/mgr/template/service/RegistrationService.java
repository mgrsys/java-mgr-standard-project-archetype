package com.mgr.template.service;

import com.magorasystems.protocolapi.response.auth.AuthResponseData;
import com.magorasystems.protocolapi.response.auth.StringAuthInfo;
import com.mgr.template.domain.command.CreateUserCommand;

import javax.validation.Valid;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
public interface RegistrationService {

    AuthResponseData<StringAuthInfo> createUser(@Valid CreateUserCommand user);

}
