package com.mgr.template.service;

public final class AppResponseCodes {

    private AppResponseCodes() {
    }

    public static final String ERROR_ID_INVALID = "template.id_invalid";

}
