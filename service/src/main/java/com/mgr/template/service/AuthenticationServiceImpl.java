package com.mgr.template.service;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;


import com.magorasystems.authapi.exception.UpdateAccessTokenException;
import com.magorasystems.authapi.exception.WrongCredentialsDetailsException;
import com.magorasystems.authapi.token.Token;
import com.magorasystems.protocolapi.request.auth.ClientAuthMeta;
import com.magorasystems.protocolapi.request.auth.ClientAuthRequest;
import com.magorasystems.protocolapi.response.auth.AuthResponseData;
import com.magorasystems.protocolapi.response.auth.StringAuthInfo;
import com.mgr.template.domain.SystemUser;
import com.mgr.template.domain.jackson.EncodedIdHelper;
import com.mgr.template.domain.jackson.EncodedIdType;
import com.mgr.template.service.creator.ApplicationTokenCreator;
import com.mgr.template.service.entity.LoginPasswordPrincipalEntity;
import com.mgr.template.service.entity.PrincipalSecretEntity;
import com.mgr.template.service.entity.UserEntity;
import com.mgr.template.service.repository.LoginPasswordPrincipalRepository;
import com.mgr.template.service.repository.PrincipalSecretRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@Service
@Validated
public class AuthenticationServiceImpl implements AuthenticationService {

    private final LoginPasswordPrincipalRepository loginPasswordPrincipalRepository;
    private final PrincipalSecretRepository principalSecretRepository;
    private final PasswordEncoder passwordEncoder;
    private final ApplicationTokenCreator tokenCreator;
    private final EncodedIdHelper encodedIdHelper;

    @Autowired
    public AuthenticationServiceImpl(LoginPasswordPrincipalRepository loginPasswordPrincipalRepository,
                                     PrincipalSecretRepository principalSecretRepository,
                                     PasswordEncoder passwordEncoder,
                                     ApplicationTokenCreator tokenCreator,
                                     EncodedIdHelper encodedIdHelper
    ) {
        this.tokenCreator = tokenCreator;
        this.loginPasswordPrincipalRepository = loginPasswordPrincipalRepository;
        this.principalSecretRepository = principalSecretRepository;
        this.passwordEncoder = passwordEncoder;
        this.encodedIdHelper = encodedIdHelper;
    }

    @Transactional
    @Override
    public AuthResponseData<StringAuthInfo> authenticateUser(ClientAuthRequest simpleAuthRequest) throws WrongCredentialsDetailsException {

        String login = simpleAuthRequest.getLogin().toLowerCase();

        LoginPasswordPrincipalEntity operator = loginPasswordPrincipalRepository.findByLogin(login);

        if (operator == null || !passwordEncoder.matches(simpleAuthRequest.getPassword(), operator.getPassword())) {
            throw new WrongCredentialsDetailsException("User doesn't exist or the password is wrong");
        }
        UserEntity user = operator.getUser();

        final Token token = tokenCreator.createToken(
                user.getId(),
                user.getEmail(),
                principalSecretRepository.findByUserId(user.getId()).getSecretCode(),
                Optional.ofNullable(simpleAuthRequest.getMeta()).map(ClientAuthMeta::getPlatform).orElse(null)
        );

        return new AuthResponseData<>(
                token.getAccessToken(),
                token.getAccessTokenExpire(),
                token.getRefreshToken(),
                new StringAuthInfo(user.getEmail(), encodedIdHelper.encode(user.getId(), EncodedIdType.USER)));
    }

    @Transactional
    @Override
    public AuthResponseData<StringAuthInfo> refreshToken(String access, String refresh) throws UpdateAccessTokenException {

        Token token = tokenCreator.refreshToken(refresh);

        return new AuthResponseData<>(
                token.getAccessToken(),
                token.getAccessTokenExpire(),
                token.getRefreshToken(),
                null
        );

    }


    @Transactional
    @Override
    public void signOutAllDevices(SystemUser key) {
        PrincipalSecretEntity one = principalSecretRepository.getOne(key.getUserId());
        one.setSecretCode(UUID.randomUUID().toString());
        one.setRefreshTime(new Date());
        principalSecretRepository.save(one);
    }

}
