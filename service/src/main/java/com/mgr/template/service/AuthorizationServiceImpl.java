package com.mgr.template.service;

import com.magorasystems.authapi.spring.security.jwt.DefaultJWTTokenReaderImpl;
import com.magorasystems.authapi.exception.InvalidTokenException;
import com.magorasystems.authapi.token.UserInfo;
import com.mgr.template.domain.SimpleSystemUser;
import com.mgr.template.domain.SystemUser;
import com.mgr.template.service.entity.PrincipalSecretEntity;
import com.mgr.template.service.repository.PrincipalSecretRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@Service
public class AuthorizationServiceImpl implements AuthorizationService {

    private final DefaultJWTTokenReaderImpl tokenReader;
    private final PrincipalSecretRepository principalSecretRepository;

    @Autowired
    public AuthorizationServiceImpl(@Value("${app.auth.token.secret}") String secret,
                                    PrincipalSecretRepository principalSecretRepository) {
        this.tokenReader = new DefaultJWTTokenReaderImpl(secret);
        this.principalSecretRepository = principalSecretRepository;
    }


    @Transactional(readOnly = true)
    @Override
    public SystemUser authorizeUser(String token) throws InvalidTokenException {

        try {

            UserInfo userInfo = tokenReader.readTokenGet(token, (userId) -> {
                PrincipalSecretEntity byUserId = principalSecretRepository.findByUserId(Long.valueOf(userId));
                return byUserId != null ? byUserId.getSecretCode() : null;
            });

            return new SimpleSystemUser(Long.valueOf(userInfo.getUserId()));

        } catch (InvalidTokenException e) {
            throw e;
        } catch (Exception e) {
            throw new InvalidTokenException(e.getMessage());
        }

    }


}
