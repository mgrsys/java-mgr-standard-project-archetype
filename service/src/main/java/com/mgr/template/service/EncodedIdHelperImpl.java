package com.mgr.template.service;

import com.mgr.template.domain.jackson.EncodedIdHelper;
import com.mgr.template.domain.jackson.EncodedIdType;
import com.mgr.template.domain.jackson.WrongIdRequestedException;
import com.mgr.template.domain.jackson.WrongIdTypeRequestedException;
import com.mgr.template.service.exception.InvalidIdException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hashids.Hashids;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
class EncodedIdHelperImpl implements EncodedIdHelper {

    private final Hashids hashids;

    @Override
    public String encode(Long value, EncodedIdType idType) {
        return idType.getWithPrefix(
                hashids.encode(value, idType.ordinal())
        );
    }

    @Override
    public Long decode(String value, EncodedIdType idType) {
        try {
            String idWithoutPrefix = EncodedIdType.parseByType(value, idType);
            long[] decode = hashids.decode(idWithoutPrefix);
            return decode[0];
        } catch (WrongIdRequestedException e) {
            throw new InvalidIdException("Wrong ID requested: " + e.getIdType() + " value: " + value);
        } catch (WrongIdTypeRequestedException e) {
            throw new InvalidIdException("Wrong ID type requested. Actual: " + e.getActualIdType() + " Requested: " + e.getRequestedIdType());
        } catch (Throwable e) {
            throw new InvalidIdException("Wrong ID requested with value: " + value);
        }
    }

}
