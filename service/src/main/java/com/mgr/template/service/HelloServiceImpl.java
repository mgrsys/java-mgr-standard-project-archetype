package com.mgr.template.service;

import com.magorasystems.protocolapi.exception.impl.ResourceForbiddenException;
import com.mgr.template.domain.Hello;
import com.mgr.template.service.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@Service
@RequiredArgsConstructor
public class HelloServiceImpl implements HelloService {

    private static final String USER_MY_ID = "my";
    private static final String HELLO_MESSAGE = "Hello! %s";

    private final UserRepository userRepository;

    @Override
    public Hello sayHello(Long userId) {
        return userRepository.findById(userId)
                .map(userEntity -> new Hello(userEntity.getId(), String.format(HELLO_MESSAGE, userEntity.getEmail())))
                .orElseThrow(() -> new ResourceForbiddenException(USER_MY_ID));
    }

}
