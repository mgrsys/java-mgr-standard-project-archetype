package com.mgr.template.service;

import java.time.Instant;

import com.magorasystems.authapi.token.Token;
import com.magorasystems.authapi.token.TokenCreator;
import com.magorasystems.authapi.token.UserInfo;
import com.magorasystems.protocolapi.exception.impl.ResourceAlreadyExistException;
import com.magorasystems.protocolapi.response.auth.AuthResponseData;
import com.magorasystems.protocolapi.response.auth.StringAuthInfo;
import com.mgr.template.domain.jackson.EncodedIdHelper;
import com.mgr.template.domain.jackson.EncodedIdType;
import com.mgr.template.service.creator.LoginPrincipalCreator;
import com.mgr.template.domain.command.CreateUserCommand;
import com.mgr.template.service.entity.LoginPasswordPrincipalEntity;
import com.mgr.template.service.entity.UserEntity;
import com.mgr.template.service.repository.LoginPasswordPrincipalRepository;
import com.mgr.template.service.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@Service
@Validated
public class RegistrationServiceImpl implements RegistrationService {

    private final UserRepository userRepository;
    private final LoginPrincipalCreator loginPrincipalCreator;
    private final LoginPasswordPrincipalRepository loginPasswordPrincipalRepository;
    private final TokenCreator tokenCreator;
    private final EncodedIdHelper encodedIdHelper;

    @Autowired
    public RegistrationServiceImpl(UserRepository userRepository, LoginPrincipalCreator loginPrincipalCreator, LoginPasswordPrincipalRepository loginPasswordPrincipalRepository, TokenCreator tokenCreator, EncodedIdHelper encodedIdHelper) {
        this.userRepository = userRepository;
        this.loginPrincipalCreator = loginPrincipalCreator;
        this.loginPasswordPrincipalRepository = loginPasswordPrincipalRepository;
        this.tokenCreator = tokenCreator;
        this.encodedIdHelper = encodedIdHelper;
    }


    @Transactional
    @Override
    public AuthResponseData<StringAuthInfo> createUser(CreateUserCommand user) {

        if (loginPasswordPrincipalRepository.findByLogin(user.getEmail().toLowerCase()) != null) {
            throw new ResourceAlreadyExistException();
        }

        UserEntity userEntity = new UserEntity();
        userEntity.setCreatedAt(Instant.now());
        userEntity.setEmail(user.getEmail());

        userEntity = userRepository.save(userEntity);

        LoginPasswordPrincipalEntity principal =
                loginPrincipalCreator.create(userEntity.getId(), user.getEmail(), user.getPassword());

        loginPasswordPrincipalRepository.save(principal);

        UserInfo.UserInfoBuilder secret = UserInfo.builder()
                .userId(String.valueOf(principal.getUserId()))
                .secret(principal.getSecret().getSecretCode());

        if (user.getMeta() != null) {
            if (user.getMeta().getPlatform() != null) {
                secret.platform(user.getMeta().getPlatform());
            }
        }

        Token token = tokenCreator.createToken(secret.build());

        return new AuthResponseData<>(
                token.getAccessToken(),
                token.getAccessTokenExpire(),
                token.getRefreshToken(),
                new StringAuthInfo(principal.getLogin(), encodedIdHelper.encode(principal.getUserId(), EncodedIdType.USER)));

    }

}
