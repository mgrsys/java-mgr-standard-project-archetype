package com.mgr.template.service.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;

@ConfigurationProperties("app.auth.token")
@Data
public class AuthTokenProps {

    private String secret;

    private Duration mobileTokenLifeDuration;

    private Duration webTokenLifeDuration;

    private Duration refreshTokenLifeDuration;

}
