package com.mgr.template.service.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("app.hashids")
@Data
public class HashidsProps {

    private String salt;

    private int minHashLength;

}
