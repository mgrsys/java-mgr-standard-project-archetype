package com.mgr.template.service.config;

import com.magorasystems.authapi.spring.security.jwt.DefaultJwtTokenCreator;
import com.magorasystems.authapi.token.TokenCreator;
import org.hashids.Hashids;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import java.time.Duration;

@EnableConfigurationProperties({HashidsProps.class, AuthTokenProps.class})
@Configuration
class ServicesConfig {

    @Bean
    public Hashids hashids(
            HashidsProps props
    ) {
        String salt = props.getSalt();
        int minHashLength = props.getMinHashLength();
        return new Hashids(salt, minHashLength);
    }

    @Bean
    public MethodValidationPostProcessor validationPostProcessor() {
        return new MethodValidationPostProcessor();
    }

    @Bean
    public TokenCreator tokenCreator(
            AuthTokenProps props
    ) {
        DefaultJwtTokenCreator defaultJwtTokenCreator = new DefaultJwtTokenCreator(props.getSecret());
        defaultJwtTokenCreator.setMobileTokenLifeMin(durationToMinutes(props.getMobileTokenLifeDuration()));
        defaultJwtTokenCreator.setWebTokenLifeTime(durationToMinutes(props.getWebTokenLifeDuration()));
        defaultJwtTokenCreator.setRefreshTokenLifeTime(durationToMinutes(props.getRefreshTokenLifeDuration()));
        defaultJwtTokenCreator.setAlwaysRefresh(false);
        return defaultJwtTokenCreator;
    }

    private int durationToMinutes(Duration duration) {
        return (int) duration.toMinutes();
    }

}
