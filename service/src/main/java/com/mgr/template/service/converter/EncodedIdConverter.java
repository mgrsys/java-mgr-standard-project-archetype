package com.mgr.template.service.converter;

import com.mgr.template.domain.jackson.EncodedId;
import com.mgr.template.domain.jackson.EncodedIdHelper;
import com.mgr.template.domain.jackson.EncodedIdType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalGenericConverter;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Set;

@Component
@Slf4j @RequiredArgsConstructor
class EncodedIdConverter implements ConditionalGenericConverter {

    private final Class<? extends EncodedId> annotationType = EncodedId.class;
    private final EncodedIdHelper encodedIdHelper;

    @Override
    public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
        return targetType.hasAnnotation(this.annotationType);
    }

    @Override
    public Set<ConvertiblePair> getConvertibleTypes() {
        return Collections.singleton(new ConvertiblePair(String.class, Long.class));
    }

    @Override
    public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
        EncodedId ann = targetType.getAnnotation(this.annotationType);
        if (ann == null) {
            throw new IllegalStateException(
                    "Expected [" + this.annotationType.getName() + "] to be present on " + targetType);
        }
        EncodedIdType idType = ann.value();
        return encodedIdHelper.decode((String) source, idType);
    }
}
