package com.mgr.template.service.creator;

import com.magorasystems.authapi.token.Token;
import com.magorasystems.authapi.exception.UpdateAccessTokenException;


/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
public interface ApplicationTokenCreator {

    Token createToken(long userId, String nick, String secret, String platform);

    Token refreshToken(String refreshToken) throws UpdateAccessTokenException;

}
