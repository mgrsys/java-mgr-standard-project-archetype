package com.mgr.template.service.creator;

import java.util.HashMap;
import java.util.Map;


import com.magorasystems.authapi.token.Token;
import com.magorasystems.authapi.token.TokenCreator;
import com.magorasystems.authapi.token.UserInfo;
import com.magorasystems.authapi.exception.UpdateAccessTokenException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@Component
public class ApplicationTokenCreatorImpl implements ApplicationTokenCreator {

    private final TokenCreator tokenCreator;

    @Autowired
    public ApplicationTokenCreatorImpl(TokenCreator tokenCreator) {
        this.tokenCreator = tokenCreator;
    }

    @Override
    public Token createToken(long userId, String nick, String secret, String platform) {

        UserInfo.UserInfoBuilder builder = UserInfo.builder();
        builder.secret(secret);
        builder.userId(String.valueOf(userId));

        Map<String, String> map = new HashMap<>();

        builder.platform(platform);

        builder.data(map);

        return tokenCreator.createToken(builder.build());
    }

    @Override
    public Token refreshToken(String refreshToken) throws UpdateAccessTokenException {
        return tokenCreator.refreshToken(refreshToken);
    }

}
