package com.mgr.template.service.creator;


import com.mgr.template.service.entity.LoginPasswordPrincipalEntity;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 * <p>
 */
public interface LoginPrincipalCreator {

    LoginPasswordPrincipalEntity create(long userId, String email, String password);

}
