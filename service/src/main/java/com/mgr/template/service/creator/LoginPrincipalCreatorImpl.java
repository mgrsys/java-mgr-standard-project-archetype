package com.mgr.template.service.creator;

import java.time.Instant;

import com.mgr.template.service.entity.LoginPasswordPrincipalEntity;
import com.mgr.template.service.entity.PrincipalSecretEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@Component
public class LoginPrincipalCreatorImpl implements LoginPrincipalCreator {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public LoginPrincipalCreatorImpl(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public LoginPasswordPrincipalEntity create(long userId, String email, String password) {
        LoginPasswordPrincipalEntity newLoginPasswordPrincipalEntity = new LoginPasswordPrincipalEntity();
        newLoginPasswordPrincipalEntity.setCreatedAt(Instant.now());
        newLoginPasswordPrincipalEntity.setPassword(passwordEncoder.encode(password));
        newLoginPasswordPrincipalEntity.setUserId(userId);
        newLoginPasswordPrincipalEntity.setLogin(email.toLowerCase());

        PrincipalSecretEntity pse = new PrincipalSecretEntity();
        pse.setRefreshTime(new Date());
        pse.setSecretCode(UUID.randomUUID().toString());
        pse.setUserId(userId);

        newLoginPasswordPrincipalEntity.setSecret(pse);

        return newLoginPasswordPrincipalEntity;
    }
}
