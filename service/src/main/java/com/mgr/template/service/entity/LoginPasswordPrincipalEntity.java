package com.mgr.template.service.entity;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;


/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@Getter
@Setter
@EqualsAndHashCode
@ToString(callSuper = true)
@NoArgsConstructor
@Entity(name = "login_password_principal")
public class LoginPasswordPrincipalEntity {

    @Id
    @Column(name = "user_id")
    private Long userId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private UserEntity user;

    @Column(name = "login", length = 32)
    private String login;


    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "created_at", nullable = false, updatable = false)
    private Instant createdAt;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private PrincipalSecretEntity secret;

}
