package com.mgr.template.service.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
@Entity(name = "principal_secret")
@Table
public class PrincipalSecretEntity {

    @Id
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "secret_code", nullable = false)
    private String secretCode;

    @Column(name = "refresh_time", nullable = false)
    private Date refreshTime;


}
