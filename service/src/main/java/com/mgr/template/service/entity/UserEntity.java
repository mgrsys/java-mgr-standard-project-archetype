package com.mgr.template.service.entity;

import com.magorasystems.hibernate.pg.entity.EntityConstants;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.Instant;


/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
@Entity(name = "sys_users")
@Table(name = "sys_users")
@DynamicUpdate
public class UserEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = EntityConstants.SEQUENCE_GENERATOR_NAME)
    private Long id;

    @Column(name = "created_at", nullable = false)
    private Instant createdAt;

    @Column(name = "email", nullable = false, length = 32)
    private String email;
}
