package com.mgr.template.service.exception;

import com.magorasystems.protocolapi.exception.impl.CodeAwareRuntimeException;

import static com.mgr.template.service.AppResponseCodes.ERROR_ID_INVALID;

/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
public class InvalidIdException extends CodeAwareRuntimeException {


    public InvalidIdException(String message) {
        this(null, message);
    }

    public InvalidIdException(String field, String message) {
        super(ERROR_ID_INVALID, field, message);
    }

}
