package com.mgr.template.service.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import com.mgr.template.service.entity.LoginPasswordPrincipalEntity;


/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
public interface LoginPasswordPrincipalRepository extends JpaRepository<LoginPasswordPrincipalEntity, Long> {

    LoginPasswordPrincipalEntity findByLogin(String login);

}