package com.mgr.template.service.repository;


import com.mgr.template.service.entity.PrincipalSecretEntity;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
public interface PrincipalSecretRepository extends JpaRepository<PrincipalSecretEntity, Long> {

    PrincipalSecretEntity findByUserId(Long userId);

}