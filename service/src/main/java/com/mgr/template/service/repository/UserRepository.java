package com.mgr.template.service.repository;


import com.mgr.template.service.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author Developed by Magora Team (magora-systems.com). 2018.
 */
public interface UserRepository extends JpaRepository<UserEntity, Long> {


}